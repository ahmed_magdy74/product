<?php
#$mysqli = new mysqli("localhost","root","alahly74","productDB");
$mysqli = new mysqli("sql113.epizy.com","epiz_30622538","HyY22pjGR3ls","epiz_30622538_ProductDB");
// Create database
$sql = "CREATE DATABASE if not exists productDB";
mysqli_query($mysqli,$sql);
$sql = "CREATE TABLE if not exists Products (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    SKU VARCHAR(30) NOT NULL UNIQUE,
    fname VARCHAR(30) NOT NULL,
    Price int NOT NULL,
    attribute VARCHAR(30)
    )";
mysqli_query($mysqli,$sql);
$sql = "SELECT * from Products Order BY id";
$result = mysqli_query($mysqli,$sql);
$products = mysqli_fetch_all($result,MYSQLI_ASSOC);
mysqli_free_result($result);
mysqli_close($mysqli);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Product List</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <style>
        .btn {
            display: inline-block;
            position: fixed;
            top: 60px;
            margin-left: 70%
        }
        body {
            margin-left: 50px;
            background-color: black;
            color: whitesmoke;
        }
        h6{
          color:black;
          text-align: center;
        }
        .footer {
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;
            background-color: lightgreen;
            color: black;
            text-align: center;
        }
    </style>
    </head>
    <script>
      function deleteFunction(event) {
        var SKU = [];
        $('.delete-checkbox:checkbox:checked').each(function() {
          SKU.push($(this).val());
        });
        var xhr = new XMLHttpRequest();
        var url = "ProductClass.php";
        xhr.open("POST", url);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        data = `SKU=${SKU}`
        xhr.send(data);
        xhr.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
              window.location.replace("Product.php");
            }
         };
      }
    </script>
<body>
    <br>
    <br>
    <h1 style="color:lightgreen;">Product List</h1>
    <button class="btn btn-success" onclick="window.location.href='AddProduct.html'">ADD</button>
    <button type="button" style="margin-left:80%" class="btn btn-warning" id="delete" onclick="return deleteFunction(event)">MASS DELETE</button>
    <hr>
    <br>
    <div class="container">
      <div class="row">
        <?php foreach($products as $product){ ?>
          <div class="col-md-3">
            <div class="card z-depth-0">
              <div class="card-content center" style="text-align: left;">
                <input class="delete-checkbox"type="checkbox" value="<?php echo htmlspecialchars($product['SKU']) ?>">
                <h6><?php echo htmlspecialchars($product['SKU']) ?></h6>
                <h6><?php echo htmlspecialchars($product['fname']) ?></h6>
                <h6><?php echo htmlspecialchars($product['Price']) ?> $</h6>
                <h6><?php echo htmlspecialchars($product['attribute']) ?></h6>
              </div>
            </div>
          </div>
        <?php } ?>
      </div>
      <div>
    </div>
    <br>
    <hr>
    <div class="footer">
      <p>Scandiweb Test Assignment</p>
    </div>
</body>
</html>